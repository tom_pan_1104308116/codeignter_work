<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 驗證碼類
 * @package     
 * @author      后盾網馬震宇 <houdunwangmzy@gmail.com>
 * 驗證碼內容文字會存到$_SESSION ['My_captcha']中
 */

class My_captcha{
    //資源
    private $img;
    //畫布寬度
    public $width = 150;
    //畫布高度
    public $height = 45;
    //背景顏色
    public $bgColor = "#ffffff";
    //驗證碼
    public $code;
    //驗證碼的隨機種子
    public $codeStr = "123456789abcdefghijklmnpqrstuvwsyz";
    //驗證碼長度
    public $codeLen = 4;
    //驗證碼字體
    public $font = "";//需要依據體環境更改路徑及檔名
    //驗證碼字體大小
    public $fontSize = 22;
    //驗證碼字體顏色
    public $fontColor = "";
    //畫線顏色
    public $lineColor = "#D0D0D0";
    //前後台驗證碼
    public $front_back="front";
    
    /**
     * 構造函數
     */
    public function __construct($arr = array()) {

        $width = '';
        $height = '';
        $codeLen = '';
        $fontSize = '';
        $bgColor = '';
        $fontColor = '';
        $lineColor = '';
        $front_back = '';

        if(!empty($arr)){
           extract($arr); 
        } 
        $this->font = BASEPATH . "fonts/captcha_font.ttf";       
        if (!is_file($this->font)) {
            my_f_fail("驗證碼字體檔案不存在");
        }
        $this->width = empty($width) ? $this->width : $width;
        $this->height = empty($height) ? $this->height : $height;
        $this->bgColor = empty($bgColor) ? $this->bgColor : $bgColor;
        $this->codeLen = empty($codeLen) ? $this->codeLen : $codeLen;
        $this->fontSize = empty($fontSize) ? $this->fontSize : $fontSize;
        $this->fontColor = empty($fontColor) ? $this->fontColor : $fontColor;
        $this->lineColor = empty($lineColor) ? $this->lineColor : $lineColor;
        $this->front_back = empty($front_back) ? $this->front_back : $front_back;
        $this->create();//生成驗證碼
    }

    /**
     * 生成驗證碼
     */
    private function createCode() {
        $code = '';
        for ($i = 0; $i < $this->codeLen; $i++) {
            $code .= $this->codeStr [mt_rand(0, strlen($this->codeStr) - 1)];
        }
        $this->code = strtoupper($code);
        if(!isset($_SESSION)){
            session_start();
        }
        if($this->front_back=="back"){
            $_SESSION ['My_admin_captcha'] = $this->code;
        }else{
            $_SESSION ['My_captcha'] = $this->code;
        }
        
    }

    /**
     * 返回驗證碼
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * 建畫布
     */
    public function create() {
        if (!$this->checkGD())
            return false;
        $w = $this->width;
        $h = $this->height;
        $bgColor = $this->bgColor;
        $img = imagecreatetruecolor($w, $h);
        $bgColor = imagecolorallocate($img, hexdec(substr($bgColor, 1, 2)), hexdec(substr($bgColor, 3, 2)), hexdec(substr($bgColor, 5, 2)));
        imagefill($img, 0, 0, $bgColor);
        $this->img = $img;
        $this->createLine();
        $this->createFont();
        $this->createPix();
        $this->createRec();
    }
    /**
    *  畫線
    */
    private function createLine(){
        $w = $this->width;
        $h = $this->height;
        $line_height = $h/10;
        $line_color = $this->lineColor;
        $color = imagecolorallocate($this->img, hexdec(substr($line_color, 1, 2)), hexdec(substr($line_color, 3, 2)), hexdec(substr($line_color, 5, 2)));
        for($i=0;$i<10;$i++){
            $step =$line_height*$i+2;
            imageline($this->img, 0, $step, $w,$step, $color);
        }
        $line_width = $w/10;
        for($i=0;$i<10;$i++){
            $step =$line_width*$i+2;
            imageline($this->img, $step-2, 0, $step+2,$h, $color);
        }
    }
    /**
     * 畫矩形邊框
     */
    private function createRec() {
        // imagerectangle($this->img, 0, 0, $this->width - 1, $this->height - 1, $this->fontColor);
    }

    /**
     * 寫入驗證碼文字
     */
    private function createFont() {
        $this->createCode();
        $color = $this->fontColor;
        if (!empty($color)) {
            $fontColor = imagecolorallocate($this->img, hexdec(substr($color, 1, 2)), hexdec(substr($color, 3, 2)), hexdec(substr($color, 5, 2)));
        }
        $x = ($this->width - 10) / $this->codeLen;
        for ($i = 0; $i < $this->codeLen; $i++) {
            if (empty($color)) {
                $fontColor = imagecolorallocate($this->img, mt_rand(50, 155), mt_rand(50, 155), mt_rand(50, 155));
            }
            imagettftext($this->img, $this->fontSize, mt_rand(- 30, 30), $x * $i + mt_rand(6, 10), mt_rand($this->height / 1.3, $this->height - 5), $fontColor, $this->font, $this->code [$i]);
        }
        $this->fontColor = $fontColor;
    }

    /**
     * 畫線
     */
    private function createPix() {
        $pix_color = $this->fontColor;
        for ($i = 0; $i < 50; $i++) {
            imagesetpixel($this->img, mt_rand(0, $this->width), mt_rand(0, $this->height), $pix_color);
        }

        for ($i = 0; $i < 2; $i++) {
            imageline($this->img, mt_rand(0, $this->width), mt_rand(0, $this->height), mt_rand(0, $this->width), mt_rand(0, $this->height), $pix_color);
        }
        //畫圓弧
        for ($i = 0; $i < 1; $i++) {
            // 設置畫線寬度
           // imagesetthickness($this->img, mt_rand(1, 3));
            imagearc($this->img, mt_rand(0, $this->width), mt_rand(0, $this->height), mt_rand(0, $this->width), mt_rand(0, $this->height)
                    , mt_rand(0, 160), mt_rand(0, 200), $pix_color);
        }
        imagesetthickness($this->img, 1);
    }

    /**
     * 顯示驗證碼
     */
    public function show() {
        header("Content-type:image/png");
        imagepng($this->img);
        imagedestroy($this->img);
        exit;
    }

    /**
     * 驗證GD庫是否打開imagepng函數是否可用
     */
    private function checkGD() {
        return extension_loaded('gd') && function_exists("imagepng");
    }

}