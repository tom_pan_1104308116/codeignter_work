<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_model extends CI_Model{

/* ======================================================================== */
	//insert 返回 insert_id
	public function insert_array($table,$data=array()){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function get_single_data($table,$cond){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($cond);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$row = $query->row_array(1);
	        return $row;
		}else{
			return array();
		}
	}


	public function get_where_data($table,$cond){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($cond);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$row = $query->result_array();
	        return $row;
		}else{
			return array();
		}
	}

}
