<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct ();
		date_default_timezone_set("Asia/Taipei");
		$this->load->library('session');
		$session_data=$this->session->userdata();
		if(!isset($session_data['admin_login_account'])){
			alert_to_page("請登入帳號",'admin/login');
		}else{
			$account=$session_data['admin_login_account'];
			$cond=array(
				'account'=>$account,
				'is_delete'=>0,
				'is_admin'=>1
				);
			$data=$this->common_model->get_single_data('member_apply',$cond);
			if(empty($data)){
				alert_to_page("帳號錯誤",'admin/login');
			}
		}
	}

	public function index()
	{
		
		// $data['person_function']=base_url().'admin/home/';
		// $data['person_function']=base_url().'home/test';

		
		//$this->load->view('admin/view_login_header');
	}

	public function login_page(){
		$this->load->view('admin/view_login_page');
	}

}