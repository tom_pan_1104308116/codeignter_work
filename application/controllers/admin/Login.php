<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct ();
		date_default_timezone_set("Asia/Taipei");
		$this->load->library('session');
		
	}

	public function index()
	{
		
		$this->load->view('admin/view_login_page');
		
		// $session_data=$this->session->userdata();
		// if(!isset($session_data['admin_login_account'])){
		// 	alert_to_page("請登入帳號",'admin/user/login_page');
		// }else{
		// 	$account=$session_data['admin_login_account'];
		// 	$cond=array(
		// 		'account'=>$account,
		// 		'is_delete'=>0,
		// 		'is_admin'=>1
		// 		);
		// 	$data=$this->common_model->get_single_data('member_apply',$cond);
		// 	if(empty($data)){
		// 		alert_to_page("帳號錯誤",'admin/user/login_page');
		// 	}
		// }
		// $this->load->view('admin/view_header');
		// $this->load->view('admin/view_home');
		// $this->load->view('admin/view_footer');
	}


	public function login_admin(){
		$admin_login_email          = $this->input->post('admin_login_email');
		$admin_login_password         = $this->input->post('admin_login_password');
		$admin_login_captcha          = $this->input->post('admin_login_captcha');

		$return_msg=array(
			'email'=>'',
			'account'=>'',
			'captcha'=>''
		);

		if(trim($admin_login_email)==""){
			$return_msg['email']="信箱不可為空";		
		}else{
			$check=check_is_email($admin_login_email);
			if($check!=true){
				$return_msg['email']="信箱格式錯誤";
			}
		}

		if(trim($admin_login_password)==""){
			$return_msg['password']="密碼不可為空";		
		}else{
			$check=check_eng_num_word($admin_login_password);
			if($check!=true){
				$return_msg['password']="密碼至少要包含一個英文、一個數字(不可含有中文，至少八個字)";
			}
		}


		if(trim($admin_login_captcha)==""){
			$return_msg['captcha']="驗證碼不可為空";		
		}else{
			if(strtolower($admin_login_captcha) != strtolower($_SESSION['My_admin_captcha'])){
				$return_msg['captcha']="驗證碼錯誤，請重新輸入";
			}
		}

		foreach ($return_msg as $key => $value) {
			if($value!=''){
				$return_msg=array(
					'error'=>1,
					'log'=>$return_msg
				);
				exit(json_encode($return_msg));
			}
		}

		$cond=array(
			'email'=>$admin_login_email,
			'password'=>hash('sha256', $admin_login_password),
			'is_delete'=>0,
			'is_admin'=>1
		);

		$member_info=$this->common_model->get_single_data('member_apply',$cond);
		if(empty($member_info)){
			$return_msg['email']="帳號或是密碼錯誤";
			$return_msg['password']="帳號或是密碼錯誤";
			$return_msg=array(
					'error'=>1,
					'log'=>$return_msg
				);
			exit(json_encode($return_msg));
		}else{
			$_SESSION['admin_login_account']=$member_info['account'];
			$_SESSION['admin_login_name']=$member_info['name'];
			$return_msg=array(
					'error'=>0,
					'log'=>'登入成功'
				);
			exit(json_encode($return_msg));
		}
	}

	public function logout_admin(){
		$this->session->sess_destroy();
		alert_to_page("登出成功",'admin/login');
		//$this->load->view('admin/view_login_page');
	}

}