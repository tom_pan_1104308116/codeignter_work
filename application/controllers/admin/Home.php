<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct ();
		date_default_timezone_set("Asia/Taipei");
		$this->load->library('session');
		$session_data=$this->session->userdata();
		if(!isset($session_data['admin_login_account']) || empty($session_data['admin_login_account'])){
			alert_to_page("請登入帳號",'admin/login');
		}else{
			$account=$session_data['admin_login_account'];
			$cond=array(
				'account'=>$account,
				'is_delete'=>0,
				'is_admin'=>1
				);
			$user_info=$this->common_model->get_single_data('member_apply',$cond);
			if(empty($user_info)){
				alert_to_page("帳號錯誤",'admin/login');
			}
		}
	}

	public function index()
	{
		$data['header_name']="後台首頁";
		$session_data=$this->session->userdata();
		$cond=array(
			'account'=>$session_data['admin_login_account'],
			'is_delete'=>0,
			'is_admin'=>1
			);
		$user_info=$this->common_model->get_single_data('member_apply',$cond);

		$data['user_info']=html_escape($user_info);
		$this->load->view('admin/view_header',$data);
		$this->load->view('admin/view_home');
		$this->load->view('admin/view_footer');
	}

	public function login_page(){
		$this->load->view('admin/view_login_page');
		
	}

}