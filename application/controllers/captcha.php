<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Captcha extends CI_Controller{


    public function __construct() {
        parent::__construct ();
        date_default_timezone_set("Asia/Taipei");
        $this->load->library('session');
    }
/* ======================================================================== */
/* 前台登入時的驗證碼  */

    public function index(){

        $config = array(
            /*以下進行驗證碼設定(可見application/libraries/My_capacha.php)*/
            'width' => 100,
            'bgColor' => "#7CC3B6",
            'lineColor' => "#4C9386",
            'fontColor' => "#4A4A4A"
        );

        // $config = array(
        //  'width' => 100,
        //  'bgColor' => "#4C9386",
        //  'lineColor' => "#7CC3B6",
        //  'fontColor' => "#FFFFFF"
        // );
        $this->load->library('my_captcha', $config);
        $this->my_captcha->show();

    }

/* ======================================================================== */
/* 給後台登入頁的驗證碼 */

    public function captcha_admin(){

        $config = array(
            /*以下進行驗證碼設定(可見application/libraries/My_capacha.php)*/
            'width'=>130,
            'front_back'=>'back'
        );

        $this->load->library('my_captcha', $config);
        $this->my_captcha->show();

    }


/* ======================================================================== */
/* 給下載時的驗證碼 */

    public function captcha_download(){

        $file_id = $this->uri->segment(3);
        $config = array(
            'file_id'=>$file_id
        );
        //my_f_p($_SESSION);die;
        $this->load->library('my_captcha_download', $config);
        $this->my_captcha_download->show();

    }

/* ======================================================================== */

}

