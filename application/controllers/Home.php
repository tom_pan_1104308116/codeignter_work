<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct ();
		date_default_timezone_set("Asia/Taipei");
		$this->load->library('session');
	}

	public function index()
	{
		$session_data=$this->session->userdata();
		if(!isset($session_data['login_account'])){
			$data['person_function']="javascript:check_member_login();";
		}else{
			$data['person_function']=base_url().'user/member_edit';
			$data['member_name']=$session_data['login_name'];
			$data['logout_button']=true;
		}
		$this->load->view('view_header',$data);
		$this->load->view('view_home');
		$this->load->view('view_footer');
	}

	public function test()
	{
		echo "asdfsa";
	}

}