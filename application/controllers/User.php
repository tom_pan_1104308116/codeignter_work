<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct ();
		date_default_timezone_set("Asia/Taipei");
		$this->load->library('session');
	}

	public function index()
	{
		$session_data=$this->session->userdata();
		if(!isset($session_data['login_account'])){
			$data['person_function']="javascript:check_member_login();";
		}else{
			$data['person_function']=base_url().'home/test';
		}
		$this->load->view('view_header',$data);
		//$this->load->view('view_home');
		$this->load->view('view_footer');
	}

	public function test()
	{
		echo "asdfsa";
	}


	public function register_page(){
		$session_data=$this->session->userdata();
		if(!isset($session_data['login_account'])){
			$data['person_function']="javascript:check_member_login();";
		}else{
			$data['person_function']=base_url().'home/test';
		}

		$cond=array(
			'parent_id'=>1,
			'type'=>2
		);
		$city_data=$this->common_model->get_where_data('area',$cond);
		$data['city_data']=$city_data;

		$this->load->view('view_header',$data);
		$this->load->view('view_register_page');
		$this->load->view('view_footer');
	}

	//註冊
	public function register_member_action(){
		$input_email            = $this->input->post('input_email');
		$input_account          = $this->input->post('input_account');
		$input_password         = $this->input->post('input_password');
		$input_confirm_password = $this->input->post('input_confirm_password');
		$input_name            = $this->input->post('input_name');
		$input_phone            = $this->input->post('input_phone');
		$input_captcha          = $this->input->post('input_captcha');

		$input_city   = $this->input->post('input_city');
		$input_area   = $this->input->post('input_area');
		$input_street = $this->input->post('input_street');


		$return_msg=array(
			'email'=>'',
			'account'=>'',
			'password'=>'',
			'confirm_password'=>'',
			'name'=>'',
			'phone'=>'',
			'captcha'=>''
		);

		$error_flag=true;

		if(trim($input_email)==""){
			$return_msg['email']="信箱不可為空";		
		}else{
			$check=check_is_email($input_email);
			if($check==true){
				$cond=array('email'=>$input_email,
							'is_delete'=>0);
				$data=$this->common_model->get_single_data('member_apply',$cond);
				if(!empty($data)){
					$return_msg['email']="此信箱已被註冊";
				}
			}else{
				$return_msg['email']="信箱格式錯誤";
			}
		}


		if(trim($input_account)==""){
			$return_msg['account']="帳號不可為空";		
		}else{
			$check=check_eng_num_word($input_account);
			if($check==true){
				$cond=array('account'=>$input_account,
							'is_delete'=>0);
				$data=$this->common_model->get_single_data('member_apply',$cond);
				if(!empty($data)){
					$return_msg['account']="此帳號已被註冊";
				}
			}else{
				$return_msg['account']="帳號至少要包含一個英文、一個數字(不可含有中文，至少八個字)";
			}
		}

		if(trim($input_password)==""){
			$return_msg['password']="密碼不可為空";		
		}else{
			$check=check_eng_num_word($input_password);
			if($check==true){
				if($input_password!=trim($input_confirm_password)){
					$return_msg['password']="兩次密碼輸入不同，請確認";
					$return_msg['input_confirm_password']="兩次密碼輸入不同，請確認";
				}
			}else{
				$return_msg['password']="密碼至少要包含一個英文、一個數字(不可含有中文，至少八個字)";
			}
		}

		if(trim($input_name)==""){
			$return_msg['name']="姓名不可為空";		
		}

		if(trim($input_phone)==""){
			$return_msg['phone']="手機號碼不可為空";		
		}else{
			$check=check_cell_phone($input_phone);
			if($check!=true){
				$return_msg['phone']="手機號碼格式錯誤，ex:09XXXXXXXX";
			}
		}

		if(trim($input_captcha)==""){
			$return_msg['captcha']="驗證碼不可為空";		
		}else{
			if(strtolower($input_captcha) != strtolower($_SESSION['My_captcha'])){
				$return_msg['captcha']="驗證碼錯誤，請重新輸入";
			}
		}

		foreach ($return_msg as $key => $value) {
			if($value!=''){
				$return_msg=array(
					'error'=>1,
					'log'=>$return_msg
				);
				exit(json_encode($return_msg));
			}
		}

		$data=array(
			'uuid'         => generate_uuid(),
			'account'      => $input_account,	
			'email'        => $input_email,
			'password'     => hash('sha256', $input_password),
			'name'		   => $input_name,
			'mobile_phone' => $input_phone,
			'city'		   => $input_city,
			'area'	       => $input_area,	
			'street'       => $input_street,
			'is_admin'     => 0,
			'is_delete'    => 0,
			'create_time'  => date("Y-m-d H:i:s"),
			'modify_time'  => date("Y-m-d H:i:s")
		);

		$insrt_id=$this->common_model->insert_array('member_apply',$data);
		if($insrt_id>0){
			if(!empty($return_msg)){
				$return_msg=array(
					'error'=>0,
					'log'=>$return_msg
				);
				exit(json_encode($return_msg));
			}
		}else{
			if(!empty($return_msg)){
				$return_msg=array(
					'error'=>2,
					'log'=>'註冊失敗'
				);
				exit(json_encode($return_msg));
			}
		}
	}

	//登入
	function login_member_action(){
		$login_account  = $this->input->post('login_account');
		$login_password = $this->input->post('login_password');
		$remember_me    = $this->input->post('remember_me');
		
		$return_msg=array(
			'account'=>'',
			'password'=>''
		);

		if(trim($login_password)==""){
			$return_msg['account']="帳號不可為空";		
		}

		if(trim($login_password)==""){
			$return_msg['password']="密碼不可為空";		
		}

		foreach ($return_msg as $key => $value) {
			if($value!=''){
				$return_msg=array(
					'error'=>1,
					'log'=>$return_msg
				);
				exit(json_encode($return_msg));
			}
		}
		$cond=array(
			'account'=>$login_account,
			'password'=>hash('sha256', $login_password),
			'is_admin'=>0,
			'is_delete'=>0
		);
		$member_info=$this->common_model->get_single_data('member_apply',$cond);
		if(empty($member_info)){
			$return_msg=array(
				'account'=>'登入失敗，帳號或是密碼錯誤',
				'password'=>'登入失敗，帳號或是密碼錯誤'
			);
			$return_msg=array(
					'error'=>1,
					'log'=>$return_msg
				);
			exit(json_encode($return_msg));
		}else{
			$_SESSION['login_account']=$member_info['account'];
			$_SESSION['login_name']=$member_info['name'];
			$return_msg=array(
				'error'=>0,
				'log'=>'登入成功'
			);
			exit(json_encode($return_msg));
		}
	}

	//登出
	function logout_member_action(){
		$this->session->sess_destroy();
		$return_msg=array(
			'error'=>0,
			'log'=>'登出成功'
		);
		exit(json_encode($return_msg));
	}

	function get_city_area(){
		$city_id          = $this->input->post('city_id');
		$cond=array(
			'parent_id'=>$city_id,
			'type'=>3
		);
		$area_data=$this->common_model->get_where_data('area',$cond);
		foreach ($area_data as $key => $value) {
			$temp_data = explode(" ", $value['name']);
			$area_data[$key]['area_name']=isset($temp_data[0])?$temp_data[0]:'';
			$area_data[$key]['zipcode']=isset($temp_data[1])?$temp_data[1]:'';
		}
		if(!empty($area_data)){
			$return_msg=array(
				'error'=>0,
				'log'=>$area_data
			);
		}else{
			$return_msg=array(
				'error'=>1,
				'log'=>'發生錯誤'
			);
		}
		exit(json_encode($return_msg));
	}

}