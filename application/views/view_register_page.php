<div class="mb-2 block-8">
  <div class="container">
    <div class="row justify-content-center mb-2">
      <div class="col-md-7 site-section-heading text-center pt-4">
        <h2>註冊</h2>
      </div>
    </div>
    <div class="row align-items-center">
      <div class="col-md-12 col-lg-7 mb-5">
        <form id="register_form">
          <div class="form-group">
            <label for="input_email" class="col-sm-12 control-label">信箱<span class="required" >*</span></label>
            <div class="col-sm-12">
              <input type="text" class="form-control" name="input_email" id="input_email" placeholder="請輸入信箱" required>
              <small id="email_alert" class="text-danger">
              </small>
            </div>
          </div>
          <div class="form-group">
            <label for="input_account" class="col-sm-12 control-label">帳號<span class="required" >*</span></label>
            <div class="col-sm-12">
              <input type="text" class="form-control" name="input_account" id="input_account" placeholder="請輸入帳號" required>
              <small id="account_alert" class="text-danger">
              </small>
            </div>
          </div>
          <div class="form-group">
            <label for="input_password" class="col-sm-12 control-label">密碼<span class="required" >*</span></label>
            <div class="col-sm-12">
              <input type="password" class="form-control" name="input_password" id="input_password" placeholder="請輸入密碼" required>
              <small id="password_alert" class="text-danger">
              </small>
            </div>
          </div>
          <div class="form-group">
            <label for="input_confirm_password" class="col-sm-12 control-label">確認密碼<span class="required" >*</span></label>
            <div class="col-sm-12">
              <input type="password" class="form-control" name="input_confirm_password" id="input_confirm_password" placeholder="請再次輸入密碼" required>
              <small id="confirm_password_alert" class="text-danger">
              </small>
            </div>
          </div>
          <div class="form-group">
            <label for="input_name" class="col-sm-12 control-label">姓名<span class="required" >*</span></label>
            <div class="col-sm-12">
              <input type="text" class="form-control" name="input_name" id="input_name" placeholder="請輸入姓名" required>
              <small id="name_alert" class="text-danger">
              </small>
            </div>
          </div>
          <div class="form-group">
            <label for="input_phone" class="col-sm-12 control-label">手機號碼<span class="required" >*</span></label>
            <div class="col-sm-12 mb-4">
              <input type="text" class="form-control" name="input_phone" id="input_phone" placeholder="請輸入手機號碼" required>
              <small id="phone_alert" class="text-danger">
              </small>
            </div>
          </div>
          <div class="form-row">
            <div class="input-group col-xs-12 col-sm-12">
              <label  class="col-sm-12 control-label">地址(非必填，宅配用)</label>
              <div class="form-group col-xs-6 col-md-3">
                <select name="input_city" id="input_city" class="form-control selectpicker" data-live-search="true" data-size="6" onchange="change_city();" >
                  <option value="" selected>請選擇縣市</option>
                  <?php 
                    foreach ($city_data as $key => $value) {
                      echo '<option value="'.$value['id'].'" >'.$value['name'].'</option>';
                    }
                  ?>
                </select>
              </div>
              <div class="form-group col-xs-6 col-md-3">
                <select name="input_area" id="input_area" class="form-control selectpicker" data-live-search="true" data-size="6">
                  <option selected>請選擇市區</option>
                  <div id="area_option_block"></div>
                </select>
              </div>
              <div class="form-group col-xs-12 col-md-6">
                <input type="text" class="form-control" name="input_street" id="input_street" placeholder="請輸入地址">
              </div>
              <small id="address_alert" class="text-danger">
              </small>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <label for="input_captcha" class="col-sm-12 control-label">驗證碼<span class="required" >*</span></label>
              <div class="col-sm-6 mb-4">
                <input type="text" class="form-control" name="input_captcha" id="input_captcha" placeholder="請輸入驗證碼(英文大小寫不拘)" required>
                <small id="captcha_alert" class="text-danger">
                  </small>
              </div>
              <div class="col-sm-6 mb-4">
                  <img src="<?php echo base_url();?>captcha" id="input-captcha-pic" onclick="javascript:document.getElementById('input-captcha-pic').src='<?php echo base_url();?>captcha' "/> <h9 style="margin-left: 10px;">看不清楚? 按一下圖片</h9>
              </div>
            </div>
          </div>
        </form>
        <div class="form-group">
          <div class="col-sm-3">
            <button type="button" class="btn btn-success  btn-block" onclick="submit_register();">送出</button>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-lg-5 text-center pl-md-5">
        <h2>趕快註冊加入我們吧 !</h2>
        <p>已經擁有帳號了? 快按底下登入吧</p>
        <p><a href="javascript:check_member_login();" class="btn btn-primary btn-sm">登入</a></p>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$.fn.selectpicker.Constructor.BootstrapVersion = '3';
$('select').selectpicker({
  liveSearch: true,
  style: '',
  styleBase: 'form-control'
});

function submit_register(){
  var pass_flag=true;
  var input_mail=$("#input_email").val();
  if(input_mail.trim()==""){
    $("#input_email").addClass('is-invalid');
    $("#email_alert").html('信箱不可為空');
    pass_flag=false;
  }
  else{
    var pattern = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/;  
    flag = pattern.test(input_mail); 
    if(flag) 
    { 
      $("#input_email").removeClass('is-invalid');
      $("#email_alert").html('');
    } 
    else 
    { 
      $("#input_email").addClass('is-invalid');
      $("#email_alert").html('信箱格式錯誤');
      pass_flag=false; 
    } 
  }


  var input_account=$("#input_account").val();
  if(input_account.trim()==""){
    $("#input_account").addClass('is-invalid');
    $("#account_alert").html('帳號不可為空');
  }
  else{
    var pattern = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;  
    flag = pattern.test(input_account); 
    if(flag) 
    { 
      $("#input_account").removeClass('is-invalid');
      $("#account_alert").html('');
    } 
    else 
    { 
      $("#input_account").addClass('is-invalid');
      $("#account_alert").html('帳號至少要包含一個英文、一個數字(不可含有中文，至少八個字)');
      pass_flag=false; 
    } 
  }
  

  var input_password=$("#input_password").val();
  var input_confirm_password=$("#input_confirm_password").val();
  if(input_password.trim()==""){
    $("#input_password").addClass('is-invalid');
    $("#password_alert").html('密碼不可為空');
    pass_flag=false;
  }
  else{
    var pattern = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;  
    flag = pattern.test(input_password); 
    if(flag) 
    { 
      if(input_password==input_confirm_password.trim()){
        $("#input_password").removeClass('is-invalid');
        $("#input_confirm_password").removeClass('is-invalid');
        $("#password_alert").html('');
        $("#confirm_password_alert").html('');
      }else{
        $("#input_password").addClass('is-invalid');
        $("#password_alert").html('兩次密碼輸入不同，請確認');
        $("#input_confirm_password").addClass('is-invalid');
        $("#confirm_password_alert").html('兩次密碼輸入不同，請確認');
      }   
    } 
    else 
    { 
      $("#input_password").addClass('is-invalid');
      $("#password_alert").html('密碼至少要包含一個英文、一個數字(不可含有中文，至少八個字)');
      pass_flag=false; 
    } 
  }

  var input_name=$("#input_name").val();
  if(input_password.trim()==""){
    $("#input_name").addClass('is-invalid');
    $("#name_alert").html('手機號碼不可為空');
    pass_flag=false;
  }

  var input_phone=$("#input_phone").val();
  if(input_password.trim()==""){
    $("#input_phone").addClass('is-invalid');
    $("#phone_alert").html('手機號碼不可為空');
    pass_flag=false;
  }
  else{
    pattern = /^09[0-9]{8}$/; 
    flag = pattern.test(input_phone); 
    if(flag) 
    { 
      $("#input_phone").removeClass('is-invalid');
      $("#phone_alert").html('');
    } 
    else 
    { 
      $("#input_phone").addClass('is-invalid');
      $("#phone_alert").html('信箱格式錯誤');
      pass_flag=false; 
    } 
  }

  var input_phone=$("#input_phone").val();
  if(input_phone.trim()==""){
    $("#input_phone").addClass('is-invalid');
    $("#phone_alert").html('手機號碼不可為空');
    pass_flag=false;
  }
  else{
    pattern = /^09[0-9]{8}$/; 
    flag = pattern.test(input_phone); 
    if(flag) 
    { 
      $("#input_phone").removeClass('is-invalid');
      $("#phone_alert").html('');
    } 
    else 
    { 
      $("#input_phone").addClass('is-invalid');
      $("#phone_alert").html('手機號碼格式錯誤，ex:09XXXXXXXX');
      pass_flag=false; 
    } 
  }


  var input_captcha=$("#input_captcha").val();
  if(input_captcha.trim()==""){
    $("#input_captcha").addClass('is-invalid');
    $("#captcha_alert").html('驗證碼不可為空');
    pass_flag=false; 
  }
  else{
    $("#input_captcha").removeClass('is-invalid');
    $("#captcha_alert").html('');
  }

  if(pass_flag!=true){
    return false;
  }

  $.ajax({
    url: 'register_member_action',
    type: 'POST',
    data: $("#register_form").serialize(),
    dataType:'json',
    async:false,
    error: function(xhr) {
      alert('發生錯誤');
    },
    success: function(data) {
      if(data.error==2){
        alert(data.log);
      }else if(data.error==1){
        var error_msg=data.log;
        $.each( error_msg, function( key, value ) {
          if(value==""){
            $("#input_"+key).removeClass('is-invalid');
            $("#"+key+"_alert").html('');
          }else{
            $("#input_"+key).addClass('is-invalid');
            $("#"+key+"_alert").html(value);
          }
        });
      }else if(data.error==0){
        var error_msg=data.log;
        $.each( error_msg, function( key, value ) {
          $("#input_"+key).removeClass('is-invalid');
          $("#"+key+"_alert").html('');
        });
        alert("註冊成功");
      }
 
    }
  });
}

function change_city(){
  var input_city=$("#input_city").val();
  if(input_city!=''){
    $("#area_option_block").html('');
    $.ajax({
      url: 'get_city_area',
      type: 'POST',
      data: {
        city_id:input_city
      },
      dataType:'json',
      async:false,
      error: function(xhr) {
        alert('發生錯誤');
      },
      success: function(data) {
        if(data.error==0){
          $("#input_area").html("<option value=''>請選擇市區</option>");
          var area_data=data.log;
          $.each( area_data, function( key, value ) {
            $("#input_area").append("<option value='"+value['id']+"'>"+ value['area_name'] + "</option>");
          });
          $("#input_area").selectpicker('refresh');
        }else{
          alert(data.log);
        }

      }
    });
  }else{
    $("#input_area").html("<option value=''>請選擇市區</option>");
    $("#input_area").selectpicker('refresh');
  }
}
</script>