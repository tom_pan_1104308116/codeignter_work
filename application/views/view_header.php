<!DOCTYPE html>
<html lang="zh-TW">
  <head>
    <title>購物吧 &mdash; 歡迎來到鞋的城市</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700"> 
    <!-- <link rel="stylesheet" href="<?php echo base_url();?>plugins/fonts/icomoon/style.css" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/owl.theme.default.min.css">


    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/aos.css">

    <link rel="stylesheet" href="<?php echo base_url();?>plugins/css/style.css">
    <script src="<?php echo base_url();?>plugins/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url();?>plugins/js/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>plugins/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>plugins/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>plugins/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>plugins/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url();?>plugins/js/aos.js"></script>
    <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.10/css/bootstrap-select.css">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.10/js/bootstrap-select.js"></script>
  </head>

  <body>
  <div class="site-wrap">
    <header class="site-navbar"  role="banner">
      <div class="site-navbar-top">
        <div class="container">
          <div class="row align-items-center">

            <div class="col-6 col-md-4 order-2 order-md-1 site-search-icon text-left" style="border:1px grey solid; border-radius: 30px;">
              <div action="" class="site-block-top-search">
                <span class="icon icon-search2"><i class="fas fa-search"></i></span>
                <input type="text" class="form-control border-0" placeholder="請輸入要搜尋的關鍵字" style="height: 30px;">
              </div>
            </div>
            <div class="col-12 mb-3 mb-md-0 col-md-4 order-1 order-md-2 text-center">
              <div class="site-logo">
                <a href="<?php echo base_url().'home';  ?>" class="js-logo-clone">Shoppers</a>
              </div>
            </div>
            <input type="hidden" >
            <div class="col-6 col-md-4 order-3 order-md-3 text-right">
              <div class="site-top-icons">
                <ul>
                  <li><a href="<?php echo $person_function; ?>"><?php echo isset($member_name)?$member_name:'';?><i class="fas fa-user"></i></a></li>
                  <li><a href="<?php echo base_url().'home/test';  ?>"><i class="far fa-heart"></i></a></li>
                  <li>
                    <a href="cart.html" class="site-cart">
                      <i class="fas fa-shopping-cart"></i>
                      <span class="count">2</span>
                    </a>
                  </li> 
                  <?php 
                  if(isset($logout_button) && $logout_button==true){
                    echo '<li><a href="javascript:confirm_logout();"><i class="fas fa-sign-out-alt"></i></a></li>';
                  }
                  ?>
                  
                  <li class="d-inline-block d-md-none ml-md-0"><a href="#" class="site-menu-toggle js-menu-toggle"><i class="fas fa-align-justify"></i></a></li>
                </ul>
              </div> 
            </div>

          </div>
        </div>
      </div> 
      <nav class="site-navigation text-right text-md-center" role="navigation">
        <div class="container">
          <ul class="site-menu js-clone-nav d-none d-md-block">
            <!-- <li class="has-children active">
              <a href="index.html">首頁</a>
              <ul class="dropdown">
                <li><a href="#"></a></li>
                <li><a href="#">Menu Two</a></li>
                <li><a href="#">Menu Three</a></li>
                <li class="has-children">
                  <a href="#">Sub Menu</a>
                  <ul class="dropdown">
                    <li><a href="#">Menu One</a></li>
                    <li><a href="#">Menu Two</a></li>
                    <li><a href="#">Menu Three</a></li>
                  </ul>
                </li>
              </ul>
            </li> -->
            <li class="has-children">
              <a href="about.html">導覽</a>
              <ul class="dropdown">
                <li><a href="#">關於我們</a></li>
                <li><a href="#">相關新聞</a></li>
                <li><a href="#">Menu Three</a></li>
              </ul>
            </li>
            <li class="has-children">
              <a href="shop.html">購物商品</a>
              <ul class="dropdown">
                <li><a href="#">男性</a></li>
                <li><a href="#">女性</a></li>
                <li><a href="#">小孩</a></li>
              </ul>
            </li>
            <li><a href="#">Catalogue</a></li>
            <li><a href="#">New Arrivals</a></li>
            <li><a href="contact.html">Contact</a></li>
          </ul>
        </div>
      </nav>
    </header>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">登入</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="col-sm-12 control-label">帳戶(信箱) 
            <h8 style="margin-left: 20px; font-size: 10px;">還沒註冊? 趕快來</h8>
            <a href="<?php echo base_url();?>user/register_page">註冊</a>
            <h8 style="font-size: 10px;">吧</h8>
          </label>

          <div class="col-sm-10">
            <input type="text" class="form-control" id="login_account" placeholder="請輸入帳號">
            <small id="login_account_alert" class="text-danger">
              </small>
          </div>
        </div>
        <div class="form-group">
          <label for="login_password" class="col-sm-12 control-label">密碼</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" id="login_password" placeholder="請輸入密碼">
            <small id="login_password_alert" class="text-danger">
              </small>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
              <label>
                <input type="checkbox" id="remember_me" > Remember me
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="submit_login();" >送出</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

  
    function check_member_login(){
        $("#myModal").modal('show'); 
    }

    function submit_login(){
      // var check=false;
      var login_account=$("#login_account").val();
      // if(login_account.trim()==""){
      //   $("#login_account").addClass('is-invalid');
      //   $("#login_account_alert").html('帳號不可為空');
      //   check=true;
      // }


      var login_password=$("#login_password").val();
      // if(login_password.trim()==""){
      //   $("#login_password").addClass('is-invalid');
      //   $("#login_password_alert").html('密碼不可為空');
      //   check=true;
      // }

       var remember_me=0;
      // if($("#remember_me").prop('checked')){
      //   remember_me=1;
      // }

      // if(check==true){
      //   return false;
      // }

      $.ajax({
        url: '<?php echo base_url();?>user/login_member_action',
        type: 'POST',
        data: {
          login_account : login_account,
          login_password : login_password,
          remember_me : remember_me
        },
        dataType:'json',
        async:false,
        error: function(xhr) {
          alert('發生錯誤');
        },
        success: function(data) {
          if(data.error==1){
            var error_msg=data.log;
            $.each( error_msg, function( key, value ) {
              if(value==""){
                $("#login_"+key).removeClass('is-invalid');
                $("#login_"+key+"_alert").html('');
              }else{
                $("#login_"+key).addClass('is-invalid');
                $("#login_"+key+"_alert").html(value);
              }
            });
          }else if(data.error==0){
            alert(data.log);
            document.location.href="<?php echo base_url();?>";
          }
          
        }
      });
    }

    function confirm_logout(){
      var check=confirm("是否確定登出?")
      if (check==true){
        $.ajax({
          url: '<?php echo base_url();?>User/logout_member_action',
          type: 'POST',
          dataType:'json',
          async:false,
          error: function(xhr) {
            alert('發生錯誤');
          },
          success: function(data) {
            alert(data.log);
            document.location.href="<?php echo base_url();?>";
          }
        });
      }

    }
       
</script>
