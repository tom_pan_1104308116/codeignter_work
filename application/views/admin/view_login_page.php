<!DOCTYPE html>
<html lang="zh-TW">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>後台登入頁</title>

  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url();?>plugins/css/sb-admin-2.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">歡迎回來，請登入!</h1>
                  </div>
                  <form id="login_admin" class="user">
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user" name="admin_login_email" id="admin_login_email" aria-describedby="emailHelp" placeholder="請輸入E-mail" required>
                      <small id="admin_login_email_alert" class="text-danger">
                        </small>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" name="admin_login_password" id="admin_login_password" placeholder="請輸入密碼" required>
                      <small id="admin_login_password_alert" class="text-danger">
                        </small>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                          <input type="text" class="form-control form-control-user" name="admin_login_captcha" id="admin_login_captcha" placeholder="請輸入驗證碼(英文大小寫不拘)" required>
                            <img src="<?php echo base_url();?>captcha/captcha_admin" id="input-captcha-pic" onclick="javascript:document.getElementById('input-captcha-pic').src='<?php echo base_url();?>captcha/captcha_admin' "/>

                      </div>
                      <small id="admin_login_captcha_alert" class="text-danger">
                        </small>
                    </div>
                    <!-- <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="remember_me">
                        <label class="custom-control-label" for="remember_me">Remember Me</label>
                      </div>
                    </div> -->
                    <a href="javascript:login_admin();" class="btn btn-primary btn-user btn-block">
                      登入
                    </a>
                    <hr>
                    <a href="index.html" class="btn btn-google btn-user btn-block">
                      <i class="fab fa-google fa-fw"></i> 以Google帳號登入
                    </a>
                    <a href="index.html" class="btn btn-facebook btn-user btn-block">
                      <i class="fab fa-facebook-f fa-fw"></i> 以Facebook帳號登入
                    </a>
                  </form>
                  <hr>
                  <!-- <div class="text-center">
                    <a class="small" href="forgot-password.html">Forgot Password?</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="register.html">Create an Account!</a>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>plugins/js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url();?>plugins/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url();?>plugins/js/sb-admin-2.js"></script>
  <script type="text/javascript">
  
  function login_admin(){
    
    $.ajax({
      url: '<?php echo base_url();?>admin/login/login_admin',
      type: 'POST',
      data: $("#login_admin").serialize(),
      dataType:'json',
      async:false,
      error: function(xhr) {
        //console.log(xhr);
        alert('發生錯誤');
      },
      success: function(data) {
        if(data.error==1){
            var error_msg=data.log;
            $.each( error_msg, function( key, value ) {
              if(value==""){
                $("#admin_login_"+key).removeClass('is-invalid');
                $("#admin_login_"+key+"_alert").html('');
              }else{
                $("#admin_login_"+key).addClass('is-invalid');
                $("#admin_login_"+key+"_alert").html(value);
              }
            });
          }else if(data.error==0){
            document.location.href="<?php echo base_url();?>admin/home";
          }
      }
    });
  }

  </script>
</body>

</html>
